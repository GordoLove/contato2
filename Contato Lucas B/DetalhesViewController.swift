//
//  DetalhesViewController.swift
//  Contato Lucas B
//
//  Created by COTEMIG on 01/09/22.
//

import UIKit

protocol DetalhesViewControllerDelegate {
    func excluirContato(index: Int)
}

class DetalhesViewController: UIViewController {
    
    @IBOutlet weak var nomeLabel: UILabel!
    @IBOutlet weak var numeroLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var enderecoLabel: UILabel!
    
    
    
    @IBAction func excluirContatoTap(_ sender: Any) {
        
        showAlert()
    }
    
    func showAlert() {
        let alert = UIAlertController(title: "Excluir Contato", message: "Sem Volta", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: { action in
            print("Cancel")
        }))
        
        alert.addAction(UIAlertAction(title: "Excluir", style: .destructive, handler: { action in
            print("Excluido")
            self.delegate?.excluirContato(index: self.index!)
            self.navigationController?.popViewController(animated: true)
        }))
        
        present(alert, animated: true)
    }
    
    
    
    public var index: Int?
    public var contato: Contato?
    public var delegate: DetalhesViewControllerDelegate?
    public var contatoDelegate: ContatoViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
        
        title = contato?.nome
        
        nomeLabel.text = contato?.nome
        numeroLabel.text = contato?.numero
        emailLabel.text = contato?.email
        enderecoLabel.text = contato?.endereco
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "editarContato"{
            let contatoViewController = segue.destination as? ContatoViewController
            contatoViewController?.contato = contato
            contatoViewController?.delegate = self
        }
    }
}

extension DetalhesViewController: ContatoViewControllerDelegate{
    func salvarNovoContato(contato: Contato) {
        
    }
    
    func editarContato() {
        title = contato?.nome
        
        nomeLabel.text = contato?.nome
        numeroLabel.text = contato?.numero
        emailLabel.text = contato?.email
        enderecoLabel.text = contato?.endereco
        
        contatoDelegate?.editarContato()
    }
}
