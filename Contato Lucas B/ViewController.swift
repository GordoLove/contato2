//
//  ViewController.swift
//  Contato Lucas B
//
//  Created by COTEMIG on 13/01/44 AH.
//

import UIKit

class Contato: Codable {
    var nome: String
    var numero: String
    var email: String
    var endereco: String
    
    init(nome: String, numero: String, email: String, endereco:String) {
        self.nome = nome
        self.numero = numero
        self.email = email
        self.endereco = endereco
    }
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var listaDeContatos:[Contato] = []
    
    var userDefaults = UserDefaults.standard
    let listaDeContatosKey = "ListadeContatos"
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeContatos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as!MyCell
        let contato = listaDeContatos[indexPath.row]
        
        cell.nome.text = contato.nome
        cell.numero.text = contato.numero
        cell.email.text = contato.email
        cell.endereco.text = contato.endereco
        
        return cell
    }
    
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        let listaData = userDefaults.value(forKey: listaDeContatosKey) as! Data
        
        do {
            let listaContatos = try JSONDecoder().decode([Contato].self, from: listaData)
            listaDeContatos = listaContatos
            tableView.reloadData()
        }catch {
            print("Error em converter os dados: \(error.localizedDescription)")
        }
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "abrirDetalhes"{
        let detalhesViewController = segue.destination as! DetalhesViewController
        let index = sender as! Int
        let contato = listaDeContatos[index]
        detalhesViewController.index = index
        detalhesViewController.contato = contato
        detalhesViewController.delegate = self
        detalhesViewController.contatoDelegate = self
            
        }else if segue.identifier == "criarContato"{
            let ContatoViewController = segue.destination as! ContatoViewController
            ContatoViewController.delegate = self
            
        }
        
    }

    func salvarContatosLocal() {
        do {
            let dataLista = try JSONEncoder().encode(listaDeContatos)
            userDefaults.setValue(dataLista, forKey: listaDeContatosKey)
        } catch {
            print("Error ao salvar dados na memoria local: \(error.localizedDescription)")
        }
    }
    
    
}

extension ViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "abrirDetalhes", sender: indexPath.row)
    }
    
}

extension ViewController: ContatoViewControllerDelegate{
    func salvarNovoContato(contato: Contato) {
        listaDeContatos.append(contato)
        tableView.reloadData()
        salvarContatosLocal()
        
    }
    
    func editarContato() {
        tableView.reloadData()
        salvarContatosLocal()
    }
}

extension ViewController: DetalhesViewControllerDelegate{
    func excluirContato(index: Int) {
        listaDeContatos.remove(at: index)
        tableView.reloadData()
        salvarContatosLocal()
    }
}
