//
//  NovoContatoViewController.swift
//  Contato Lucas B
//
//  Created by COTEMIG on 01/09/22.
//

import UIKit

protocol ContatoViewControllerDelegate {
    func salvarNovoContato(contato: Contato)
    func editarContato()
}

class ContatoViewController: UIViewController {

    @IBOutlet weak var nomeField: UITextField!
    @IBOutlet weak var numeroField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var enderecoField: UITextField!
    
    public var contato: Contato?
    public var delegate: ContatoViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        nomeField.text = contato?.nome
        numeroField.text = contato?.numero
        emailField.text = contato?.email
        enderecoField.text = contato?.endereco
        
        if contato == nil {
            title = "Novo Contato"
        }else{
            title = "Editar Contato"
        }
    }
    
    
    @IBAction func salvarContato(_ sender: Any) {
        if contato == nil{
            let contato = Contato(nome: nomeField?.text ?? " ",
                                  numero: numeroField?.text ?? "",
                                  email: emailField?.text ?? "",
                                  endereco: enderecoField?.text ?? "")
            
            delegate?.salvarNovoContato(contato: contato)
        }else {
            contato?.nome = nomeField?.text ?? ""
            contato?.numero = numeroField?.text ?? ""
            contato?.email = emailField?.text ?? ""
            contato?.endereco = enderecoField?.text ?? ""
        
            delegate?.editarContato()
    }
        navigationController?.popViewController(animated: true)
        
}
    
}
